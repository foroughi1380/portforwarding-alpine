# What is this 
Port Forwarding with alpine linux and Socat using url

---
# How to use
## step 1
upload a file to the internet with direct access url contains text like below

```
    [localport]->[direct address]:[direct port]
    .
    .
    .
    .
```

for example
```
    5620->123.123.123.123:5620
    5621->123.123.123.123:5622
```

## step 2
create an instance with runtime environment variables `DATA_URL` and set your uploaded text file url

**attention** : you must bind all forwarding ports to access from outside the container

## step 3
Enjoy!!


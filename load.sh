#! /bin/sh

echo $DATA_URL

wget -O forward $DATA_URL

for rule in $(cat < forward | tr "\n" " ")
do
   address=$(sed "s/->/,reuseaddr,fork@TCP4:/g" << EOF
	$rule
EOF
)

   cmd="socat tcp4-listen:"$(echo $address | tr -d '^[:space:]' | tr '@' ' ')
   echo $cmd
   $cmd &
done

tail -F /z
